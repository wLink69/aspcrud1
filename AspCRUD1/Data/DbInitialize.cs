﻿using AspCRUD1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCRUD1.Data
{
    public class DbInitialize
    {
        public static void Initialize(AspCRUD1Context context)
        {
            if (context.Product.Any())
            {
                return;
            }

            var products = new Product[]
            {
                new Product{ProductName="product1", SupplierID=1, CategoryID=1, QuantityPerUnit=1, UnitPrice="1", UnitsInStock="1", UnitsOnOrder="1", ReorderLevel="1", Discontinued="ok"},
                new Product{ProductName="product2", SupplierID=2, CategoryID=2, QuantityPerUnit=2, UnitPrice="2", UnitsInStock="2", UnitsOnOrder="2", ReorderLevel="2", Discontinued="ok"},
                new Product{ProductName="product3", SupplierID=3, CategoryID=3, QuantityPerUnit=3, UnitPrice="3", UnitsInStock="3", UnitsOnOrder="3", ReorderLevel="3", Discontinued="ok"},
                new Product{ProductName="product4", SupplierID=4, CategoryID=4, QuantityPerUnit=4, UnitPrice="4", UnitsInStock="4", UnitsOnOrder="4", ReorderLevel="4", Discontinued="ok"}
            };
            foreach (Product p in products)
            {
                context.Product.Add(p);
            }
            context.SaveChanges();

            var categories = new Category[]
            {
                new Category{CategoryName="category1", Description="alahu akbar", Picture="no picture"},
                new Category{CategoryName="category2", Description="alahu akbar", Picture="no picture"},
                new Category{CategoryName="category3", Description="alahu akbar", Picture="no picture"},
                new Category{CategoryName="category4", Description="alahu akbar", Picture="no picture"},
            };
            foreach (Category c in categories)
            {
                context.Category.Add(c);
            }
            context.SaveChanges();
        }
    }
}
