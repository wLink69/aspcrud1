﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AspCRUD1.Models;

namespace AspCRUD1.Models
{
    public class AspCRUD1Context : DbContext
    {
        public AspCRUD1Context (DbContextOptions<AspCRUD1Context> options)
            : base(options)
        {
        }

        public DbSet<AspCRUD1.Models.Category> Category { get; set; }

        public DbSet<AspCRUD1.Models.Product> Product { get; set; }
    }
}
